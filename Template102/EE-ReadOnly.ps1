#Azure authentication variables. 
$AADTenantId="76c58198-c574-4bd9-84c3-598d38f5b8c7"
$AADApplicationId="4d2a9127-1330-4351-99a1-de0c6b3ca7e2"
$AADServicePrincipalSecret = "eqc2b2uv8a5ZjxeTfIVq42tiDooRVGNyMQfNCJz6AUg="

#login to Azure using Service principal 
$secpasswd = ConvertTo-SecureString $AADServicePrincipalSecret -AsPlainText -Force
$appcreds = New-Object System.Management.Automation.PSCredential ($AADApplicationId, $secpasswd)
Add-AzureRmAccount -ServicePrincipal -Credential $appcreds -TenantId $AADTenantId

