add-AzureRmAccount

$ResourceGroupName = "Template102"
$Location = "australia east"
$StorageName = "storagefortemplate102"
$StorageSKU = "Standard_LRS"
$DeploymentName = "Template102Deployment"
New-AzureRmResourceGroup -Name $ResourceGroupName -Location $Location
#$storageName = "templatestorage102" # + (Get-Random)
#create new storage account
New-AzureRmStorageAccount -ResourceGroupName $ResourceGroupName -AccountName $storageName -Location $Location -SkuName $StorageSKU -Kind Storage
#get account in in variablen

$accountKey = (Get-AzureRmStorageAccountKey -ResourceGroupName $ResourceGroupName -Name $storageName).Value[0]
# get-azure 
$context = New-AzureStorageContext -StorageAccountName $storageName -StorageAccountKey $accountKey 
#Create new storage container
New-AzureStorageContainer -Name "templates" -Context $context -Permission Container

#upload json files to the storage account
Set-AzureStorageBlobContent -File "C:\my-Git-repos\azure-bitbucket\Template102\CreateVMTemplate.json" -Context $context -Container "templates"
Set-AzureStorageBlobContent -File "C:\my-Git-repos\azure-bitbucket\Template102\Parameters.json" -Context $context -Container templates

$templatePath = "https://" + $storageName + ".blob.core.windows.net/templates/CreateVMTemplate.json"
$parametersPath = "https://" + $storageName + ".blob.core.windows.net/templates/Parameters.json"

New-AzureRmResourceGroupDeployment -ResourceGroupName $ResourceGroupName -Name $DeploymentName -TemplateUri $templatePath -TemplateParameterUri $parametersPath -Mode Incremental

#define template and parameter file from local drive#
$templatePathlocal = "C:\my-Git-repos\azure-bitbucket\Template102\CreateVMTemplate.json"
$parametersPathlocal = "C:\my-Git-repos\azure-bitbucket\Template102\Parameters.json"
New-AzureRmResourceGroupDeployment -ResourceGroupName $ResourceGroupName -Name $DeploymentName -TemplateFile $templatePathlocal -TemplateParameterFile $parametersPathlocal -Mode Incremental

